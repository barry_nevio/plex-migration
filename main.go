package main

import (
	"bufio"
	"context"
	"fmt"
	"math"
	"os"
	"path/filepath"
	"runtime"
	"strings"
	"time"

	//"github.com/jlaffaye/ftp"
	"github.com/inhies/go-bytesize"
	"github.com/spf13/cast"
	ftp "gitlab.com/barry_nevio/goftp"
	"gitlab.com/barry_nevio/goo-tools/Error"
)

func main() {

	basePath := "/mnt/MEDIASTORE1/Plex/TV Shows"
	remoteBasePath := "/mnt/media/MEDIASTORE1/Plex/TV Shows/"

	// Walk the path and build slice of files
	err := filepath.Walk(basePath,
		func(path string, info os.FileInfo, err error) error {

			var entries []*ftp.Entry

			// No dice
			if err != nil {
				return err
			}

			// Path Cleaned
			pathClean := strings.TrimPrefix(WinDirToLinDir(path), basePath)

			// Check if dir exists on remote
			if info.IsDir() {
				fmt.Println("DIR: " + pathClean)
				entries, err = GetRemoteEntries(remoteBasePath + pathClean)

				if len(entries) == 0 {
					fmt.Println("DIR: " + pathClean + " DOES NOT EXIST! On Remote, CREATING!")
					MakeRemoteDir(remoteBasePath + pathClean)

				} else {
					fmt.Println("DIR: " + pathClean + " EXISTS! On Remote")
				}
			}

			// Check if file exists on remote
			if !info.IsDir() {
				fmt.Println("FILE: " + pathClean)

				// Open File
				file, err := os.Open(path)
				if err != nil {
					fmt.Println("File opening error", err.Error())
					return err
				}
				fileinfo, _ := file.Stat()

				// Get Entries
				entries, err = GetRemoteEntries(remoteBasePath + pathClean)

				// File Is good, skip
				if len(entries) > 0 && fileinfo.Size() >= cast.ToInt64(entries[0].Size) {
					fmt.Println("FILE: " + pathClean + " EXISTS! On Remote")
					file.Close()
					return nil
				}

				// File does not exist
				if len(entries) == 0 {
					fmt.Println("FILE: " + pathClean + " DOES NOT EXIST! On Remote UPLOADING! \n")
				}

				// File exist, but it's fudged
				if len(entries) > 0 && fileinfo.Size() > cast.ToInt64(entries[0].Size) {
					fmt.Println("FILE: " + pathClean + " EXISTS BUT IS FUDGED! On Remote DELETING THEN UPLOADING! \n")
					Delete(remoteBasePath + pathClean)
				}

				trans := &ftp.Transfer{}
				trans.Path = remoteBasePath + pathClean
				trans.Reader = bufio.NewReader(file)
				trans.Filesize = fileinfo.Size()
				fmt.Println("Filesize: " + cast.ToString(trans.Filesize))

				err = Upload(trans)
				if err != nil {
					fmt.Println("Error Uploading", err.Error())
					return err
				}

				file.Close()
			}
			return nil
		})

	if err != nil {
		fmt.Println("ERROR WALKING DIR: " + err.Error())
		return
	}

}

func Upload(transfer *ftp.Transfer) error {

	var err error

	conn, err := ConnectToFTP()
	if err != nil {
		return err
	}

	ctx, cancel := context.WithCancel(context.Background())

	go func(ctx context.Context) {
		err = conn.Stor(transfer)
		cancel()
		return
	}(ctx)

	// start UI loop
	t := time.NewTicker(time.Second)
	defer t.Stop()

	var writtenLast int64
	var message string
	var speed float64
	var percentage float64
	for {
		select {
		case <-t.C:
			// Clear current console line
			fmt.Printf("\r%s", strings.Repeat(" ", len(message)))

			// ** Calculate Speed in Mbps: BEGIN ** //
			// How many BYTES were written this iteration
			writtenThisIteration := transfer.WrittenTotal - writtenLast
			writtenLast = transfer.WrittenTotal
			// Multiple by 8 To get in BITS and then divide by 1048576 to get MEGA BITS
			speed = (cast.ToFloat64(writtenThisIteration) * 8) / 1048576
			// ** Calculate Speed in Mbps: END ** //

			// Calculate percentage
			percentage = math.Round((cast.ToFloat64(transfer.WrittenTotal) / cast.ToFloat64(transfer.Filesize)) * 100)

			// Make message
			message = "Transferring... " + cast.ToString(bytesize.New(cast.ToFloat64(transfer.WrittenTotal))) + "/" + cast.ToString(bytesize.New(cast.ToFloat64(transfer.Filesize))) + " | " + cast.ToString(percentage) + "%% | " + cast.ToString(speed) + "Mbp/s"
			// Replace current line with the message
			fmt.Printf("\r" + message)
		case <-ctx.Done():
			// Clear current console line
			fmt.Printf("\r%s", strings.Repeat(" ", len(message)))

			// Message
			message = "Transferred... " + cast.ToString(bytesize.New(cast.ToFloat64(transfer.WrittenTotal))) + "/" + cast.ToString(bytesize.New(cast.ToFloat64(transfer.Filesize))) + " | " + cast.ToString(percentage) + "%% | " + cast.ToString(speed) + "Mbp/s"

			// FTP Error
			if transfer.WrittenTotal != transfer.Filesize {
				_ = conn.Quit()
				return Error.New("FILE TRANSFER INCOMPLETE!! FTP ERROR: " + err.Error())
			}

			// Replace current line with the message
			fmt.Printf("\r" + message)
			fmt.Println("\n")
			// Quit ftp
			conn.Quit()
			return err
		}
	}

}

func GetRemoteEntries(entry string) ([]*ftp.Entry, error) {
	var entries []*ftp.Entry
	conn, err := ConnectToFTP()
	if err != nil {
		conn.Quit()
		return entries, err
	}
	entries, err = conn.List(entry)
	if err != nil {
		conn.Quit()
		return entries, err
	}
	conn.Quit()
	return entries, nil
}

func MakeRemoteDir(remoteDir string) error {
	conn, err := ConnectToFTP()
	if err != nil {
		conn.Quit()
		return err
	}
	err = conn.MakeDir(remoteDir)
	if err != nil {
		conn.Quit()
		return err
	}
	conn.Quit()
	return nil
}

func Delete(target string) error {
	conn, err := ConnectToFTP()
	if err != nil {
		conn.Quit()
		return err
	}
	err = conn.Delete(target)
	if err != nil {
		conn.Quit()
		return err
	}
	conn.Quit()
	return nil
}

func ConnectToFTP() (*ftp.ServerConn, error) {
	conn, err := ftp.Dial("144.217.183.138:21", ftp.DialWithTimeout(10*time.Second))
	if err != nil {
		return conn, err
	}

	err = conn.Login("joe", "FuckYou1234")

	return conn, err
}

func WinDirToLinDir(dir string) string {
	switch strings.ToUpper(runtime.GOOS) {
	case "WINDOWS":
		return strings.Replace(dir, `\`, "/", -1)
	default:
		return dir
	}

}
